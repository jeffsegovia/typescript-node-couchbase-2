import * as dotenv from "dotenv";
import express, { Request, Response, NextFunction } from "express";
import helmet from "helmet";
import cors from "cors";

dotenv.config();

import todoRouter from "./routes/todo";
import errorHandler from "./middlewares/error";

const app = express();

app.use(helmet());
app.use(cors());
app.use(express.json());

app.get("/health-check", (req: Request, res: Response, next: NextFunction) => {
  res.status(200).json("The API is working fine!");
});

// ROUTES
app.use("/api/todos", todoRouter);

app.use(errorHandler);

app.listen(3100, () => {
  console.log("Server running at PORT 3100.");
});
