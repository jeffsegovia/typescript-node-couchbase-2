import moment from "moment";
import { v4 as genId } from "uuid";

export const createId = (docType: string) => `${docType.toUpperCase()}::${genId()}`;

export const getDateToday = () => moment().format("YYYY-MM-DD HH:mm:ss.SSSZ");
