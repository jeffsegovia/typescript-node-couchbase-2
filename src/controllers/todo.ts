import { Request, Response, NextFunction } from "express";
import ErrorResponse from "../models/errorResponse";
import TodoModel from "../models/todo";

const getAllTodos = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const todos = await TodoModel.getAll();

    res.status(200).json({ status: "success", count: todos.length, data: todos });
  } catch (err) {
    next(err);
  }
};

const getTodoById = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const todo = await TodoModel.findById(req.params.id);

    res.status(200).json({ status: "success", data: todo });
  } catch (err) {
    next(err);
  }
};

const createTodo = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { title, isCompleted, todoType } = req.body;

    if (!title || !todoType)
      return next(new ErrorResponse("Title, isCompleted, TodoType are required", 403));

    const newTodo = await TodoModel.create({ title, isCompleted, todoType });

    res.status(201).json({ status: "success", data: newTodo });
  } catch (error) {
    next(error);
  }
};

const todoController = {
  getAllTodos,
  getTodoById,
  createTodo,
};

export default todoController;
