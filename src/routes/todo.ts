import express from "express";

import todoController from "../controllers/todo";

const router = express.Router();

router.route("/").get(todoController.getAllTodos).post(todoController.createTodo);

router.route("/:id").get(todoController.getTodoById);

export default router;
