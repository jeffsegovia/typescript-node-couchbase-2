import couchbase from "couchbase";
import { getDateToday } from "../utils/helpers";

const DB_HOST = process.env.DB_HOST;
const DB_USERNAME = process.env.DB_USERNAME;
const DB_PASSWORD = process.env.DB_PASSWORD;
const DB_BUCKET = process.env.DB_BUCKET;

const cluster = new couchbase.Cluster("http://localhost:8091");
const N1QLQuery = couchbase.N1qlQuery;

cluster.authenticate("Administrator", "jeffsegovia" as string);

const bucket = cluster.openBucket("my_app");

bucket.on("connect", () => {
  console.log(`DB Connected at http://localhost:8091`);
  console.log(`Bucket: my_app`);
});

bucket.on("error", (error) => {
  console.log(error.message);
});

function query<T>(queryString: string) {
  const q = N1QLQuery.fromString(queryString);

  return new Promise<T[]>((resolve, reject) => {
    bucket.query(q, function (error, result: T[]) {
      if (error) {
        reject(error);
      } else resolve(result);
    });
  });
}

function getById<T>(id: string, docType: string = "document") {
  return new Promise<T>((resolve, reject) => {
    bucket.get(id, (err, result) => {
      if (err) {
        if (err.code === couchbase.errors.keyNotFound) {
          return reject({
            ...err,
            message: `Cannot find ${docType} with ID: ${id}.`,
          });
        }
        return reject(err);
      }

      const document = { ...result.value, id };
      delete document.isDeleted;
      delete document.docType;

      resolve(document);
    });
  });
}

/**
 *
 * @param key id of new document
 * @param value value of new document
 * @param docType the document type
 * @description inserts a new record in the database
 */
function create<T>(key: string, value: any, docType: string) {
  return new Promise<T>((resolve, reject) => {
    const dateCreated = getDateToday();
    const dateUpdated = getDateToday();
    const data = {
      ...value,
      dateCreated,
      dateUpdated,
      docType,
      isDeleted: false,
    };

    bucket.insert(key, data, (err, result) => {
      if (err) {
        if (err.code === couchbase.errors.keyAlreadyExists)
          return reject({
            ...err,
            message: `A ${docType} with the given ID already exists.`,
          });
        return reject(err);
      }

      resolve({ id: key, ...value, dateCreated, dateUpdated });
    });
  });
}

const db = {
  query,
  getById,
  create,
};

export default db;
