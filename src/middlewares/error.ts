import couchbase from "couchbase";
import { Request, Response, NextFunction } from "express";
import ErrorResponse from "../models/errorResponse";

const errorHandler = (err: any, req: Request, res: Response, next: NextFunction) => {
  let error = { ...err };

  error.message = err.message;

  if (error.code === couchbase.errors.keyNotFound) {
    error = new ErrorResponse(error.message, 404);
  }

  res.status(error.statusCode || 500).json({
    status: "failed",
    error: err.message || "Server Error",
  });
};

export default errorHandler;
