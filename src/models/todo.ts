import db from "../db/couchbase";
import { createId } from "../utils/helpers";

export interface Todo {
  title: string;
  isCompleted: boolean;
  todoType: string;
  userId: string;
  docType: string;
  isDeleted: boolean;
  dateCreated: string;
  dateUpdated: string;
}

const getAll = async () => {
  const queryStr = `
            SELECT meta().id, t.title, t.isCompleted, t.todoType, t.dateCreated, t.dateUpdated
            FROM my_app t
            WHERE t.docType="TODO"
            AND t.isDeleted=false
    `;

  const todos = await db.query<Todo>(queryStr);

  return todos;
};

const findById = async (id: string) => {
  const todo = await db.getById<Todo>(id, "TODO");
  return todo;
};

const create = async (todo: Partial<Todo>) => {
  const id = createId("TODO");
  const newTodo = await db.create<Todo>(id, todo, "TODO");
  return newTodo;
};

const TodoModel = {
  getAll,
  findById,
  create,
};

export default TodoModel;
