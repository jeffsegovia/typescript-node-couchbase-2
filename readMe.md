# Node Todo App

A Simple Todo App for demonstrating the usage of TypeScript with Node and Couchbase

To get started, run the ff. in your terminal:

```
 npm install
```

To start the app, run:

```
npm run dev
```

-jep
